
PATH_FRONTEND	= HydrogenAppTest

PWD				= $(shell pwd)
DATE			= `date +'%Y-%m-%d'`

install:
	@test ! -f .hymn && $(MAKE) -s set-install-mode-dev || true
	@$(MAKE) -s configure
	@$(MAKE) -s composer-install
	@echo "Installing modules:" && hymn app-install
	@hymn database-load
	@echo "Settings permissions..." && $(MAKE) -s set-permissions
	@echo "Generating autoloaders..." && $(MAKE) -s composer-update

update:
	@$(MAKE) -s composer-update
	@test -e vendor/ceus-media/hydrogen-modules-nonfree && echo "Updating protected modules..." && svn up -q vendor/ceus-media/hydrogen-modules-nonfree || true
	@hymn app-update
	@$(MAKE) -s set-permissions

doc:
	@test ! -f vendor/ceus-media/doc-creator/doc-creator.php && echo "DocCreator is not installed" || true
	@test ! -f vendor/ceus-media/doc-creator/doc-creator.php && echo "No config file set for DocCreator (missing config/doc.xml)" || true
	@test -f vendor/ceus-media/doc-creator/doc-creator.php && test -f config/doc.xml && vendor/ceus-media/doc-creator/doc-creator.php --config-file=config/doc.xml || true


##  PUBLIC: PERMISSIONS
#------------------------
set-permissions:
	@$(MAKE) -s set-ownage
	@$(MAKE) -s set-rights
	@$(MAKE) -s enable-clamav

set-ownage:
	@sudo chown -R ${shell hymn config-get system.user} .
	@sudo chgrp -R ${shell hymn config-get system.group} .

set-rights:
	@find . -type d -not -path "./vendor*" -print0 | xargs -0 xargs chmod 770
	@find . -type f -not -path "./vendor*" -print0 | xargs -0 xargs chmod 660
	@find . -type f -path "./vendor/ceus-media/hydrogen-modules-nonfree/*" -print0 | xargs -0 xargs chmod 770 >/dev/null 2>&1 || true

enable-clamav:
	@cat /etc/passwd | grep clamav > /dev/null && ( groups clamav | grep ${shell hymn config-get system.group} > /dev/null && true || ( adduser clamav ${shell hymn config-get system.group} && sudo service clamav-daemon restart ) ) || true;


##  PUBLIC: SETTERS
#------------------------
set-install-mode-dev:
	@test -f .hymn.dev && cp .hymn.dev .hymn || true
	@test -f config/config.ini.dev && cp config/config.ini.dev config/config.ini || true
	@hymn config-set application.installMode dev

set-install-mode-live:
	@test -f .hymn.live && cp .hymn.live .hymn || true
	@test -f config/config.ini.live && cp config/config.ini.live config/config.ini || true
	@hymn config-set application.installMode live

set-install-mode-test:
	@test -f .hymn.test && cp .hymn.test .hymn || true
	@test -f config/config.ini.test && cp config/config.ini.test config/config.ini || true
	@hymn config-set application.installMode test

set-install-type-copy:
	@hymn config-set application.installType copy

set-install-type-link:
	@hymn config-set application.installType link


##  PUBLIC: CONFIGURATION
#------------------------
configure: detect-application-uri detect-application-url detect-sources
	@$(MAKE) -s .apply-application-url-to-config

configure-ask: detect-application-uri detect-application-url detect-sources
	@$(MAKE) -s ask-system-user
	@$(MAKE) -s ask-system-group
	@$(MAKE) -s ask-application-uri
	@$(MAKE) -s ask-application-url
	@$(MAKE) -s ask-database
	@$(MAKE) -s .apply-application-url-to-config

detect-application-uri:
	@echo
	@echo Set application URI to ${PWD}/ by detection.
	@hymn config-set application.uri ${PWD}/

detect-application-url:
	@test ! $(shell hymn config-get application.url) && $(MAKE) ask-application-url || true
	@$(MAKE) -s .apply-application-url-to-config

detect-sources:
	@hymn config-set sources.CeusMedia_Modules_Public.path $(shell hymn config-get application.uri)vendor/ceus-media/hydrogen-modules/

ask-system-group:
	@echo
	@echo "Please define the filesystem group to be allowed!"
	@hymn config-set system.group

ask-system-user:
	@echo
	@echo "Please define the filesystem user to be owner!"
	@hymn config-set system.user

ask-application-uri:
	@echo
	@echo "Please define the absolute filesystem URI to this application! Attention: Mind the trailing slash!"
	@hymn config-set application.uri

ask-application-url:
	@test ! $(shell hymn config-get application.url) && echo "Please define the absolute network URL to this application! Attention: Mind the trailing slash!" && hymn config-set application.url || true
	@$(MAKE) -s .apply-application-url-to-config

.apply-application-url-to-config:
	@hymn app-base-config-set app.base.url $(shell hymn config-get application.url) || true
	@hymn app-base-config-enable app.base.url > /dev/null || true

ask-database:
	@echo
	@echo Please configure database access! Attention: Database MUST BE existing.
	@hymn database-config


##  COMPOSER
#------------------------
composer-install:
	@test vendor && echo && echo Updating libraries: && composer update --no-dev || true
	@test ! vendor && echo && echo Loading libraries: && composer install --no-dev || true
	@$(MAKE) -s set-rights

composer-install-dev:
	@test vendor && echo && echo Updating libraries: && composer update || true
	@test ! vendor && echo && echo Loading libraries: && composer install || true
	@$(MAKE) -s set-rights

composer-install-force:
	@test vendor && rm -Rf vendor
	@composer install --no-dev
	@$(MAKE) -s set-rights

composer-update:
	@composer update --no-dev
	@$(MAKE) -s set-rights

composer-update-dev:
	@composer update
	@$(MAKE) -s set-rights
